import path from "path";
import express from "express";
import session from "express-session";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import mongo from "connect-mongo";

import mongoose from "mongoose";
import passport from "passport";
import { MONGODB_URI, SESSION_SECRET } from "./util/secrets";

import router from "./routes";

const app = express();
const MongoStore = mongo(session);

mongoose.connect(MONGODB_URI, { useNewUrlParser: true} ).then(
    () => { 
        console.log("MongoDB connection success");
    },
).catch(err => {
    console.log("MongoDB connection error. Please make sure MongoDB is running. " + err);
});

app.use(cookieParser());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: SESSION_SECRET,
    store: new MongoStore({
        url: MONGODB_URI,
        autoReconnect: true
    })
}));

passport.authenticate("local");
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.join(__dirname, "public"), { maxAge: 31557600000 }));
app.use("/files", express.static(path.join(__dirname, "../files"), { maxAge: 31557600000 }));

router(app);
export default app;
