import { Request, Response, NextFunction } from "express";
import { Cart, CartDocument } from "../models/Cart";
import { User, UserDocument } from "../models/User";
import { ReE, ReS } from "../util/common";


//middleware
export const createCart = async (req: Request, res: Response, next: NextFunction) => {
    try {
        var cookie = req.cookies.cartId;
        if(req.user) {
            const user: UserDocument = await User.findById(req.user._id).exec();
            if (cookie === undefined){
                if(user.cart) {
                    res.cookie("cartId",user.cart, { maxAge: 60*60*24*365, httpOnly: true });
                } else {
                    const doc: CartDocument = await new Cart().save();
                    user.cart = doc._id;
                    user.save();
                    res.cookie("cartId",doc._id, { maxAge: 60*60*24*365, httpOnly: true });
                }
            } else {
                const doc: CartDocument = await Cart.findById(cookie).populate({path: "products", model: "Product"}).exec();
                if(!doc) {
                    const doc: CartDocument = await new Cart().save();
                    res.cookie("cartId",doc._id, { maxAge: 60*60*24*365, httpOnly: true });
                }
            }
        } else {
            if (cookie === undefined){
                const doc: CartDocument = await new Cart().save();
                res.cookie("cartId",doc._id, { maxAge: 60*60*24*365, httpOnly: true });
                
            } else {
                const doc: CartDocument = await Cart.findById(cookie).populate({path: "products", model: "Product"}).exec();
                if(!doc) {
                    const doc: CartDocument = await new Cart().save();
                    res.cookie("cartId",doc._id, { maxAge: 60*60*24*365, httpOnly: true });
                }
            }
        }

        next();
    } catch(err) {
        next();
    } 
};

export const findAll = async (req: Request, res: Response) => {
    try {
        const doc: CartDocument[] = await Cart.find({});

        const count: number = await Cart.countDocuments({}).exec();
        return ReS(res, {doc, count});
    } catch(err) {
        return ReE(res, err);
    }
};
export const findOne = async (req: Request, res: Response) =>  {
    try {
        const doc: CartDocument = await Cart.findById(req.params.id).populate({path: "products._id", model: "Product"}).exec();
        return ReS(res, {doc});
    } catch(err) {
        return ReE(res, err);
    }   
};

export const getFrontCart = async (req: Request, res: Response) =>  {
    try {
        const doc: CartDocument = await Cart.findById(req.cookies.cartId).populate({path: "products._id", model: "Product"}).exec();
        return ReS(res, {doc});
    } catch(err) {
        return ReE(res, err);
    }   
};


export const addToCart = async (req: Request, res: Response) => {
    try {
        const cart: CartDocument = await Cart.findOne({_id: req.cookies.cartId, "products._id": req.params.id});
        if(cart) {
            const doc = await Cart.findOneAndUpdate({_id: req.cookies.cartId, "products._id": req.params.id}, {$inc: { "products.$.count": 1}}).exec();
            return await ReS(res, {doc});
        } else {
            const doc = await Cart.findByIdAndUpdate(req.cookies.cartId, { $push: { products: { _id: req.params.id}}}, {new: true}).exec();
            return await ReS(res, {doc});
        }

    } catch (err) {
        return ReE(res, err);
    }
};
export const removeFromCart = async (req: Request, res: Response) => {
    try {
        const doc: CartDocument = await Cart.findByIdAndUpdate(req.cookies.cartId, {$pull: { products:  {_id: req.params.id}}}, {new: true}).exec();

        return await ReS(res, {doc});
    } catch (err) {
        return ReE(res, err);
        
    }
};
export const destroy = async (req: Request, res: Response) => {
    try {
        const doc: CartDocument = await Cart.findByIdAndRemove(req.params.id).exec();
        return ReS(res, {doc});
    } catch(err) {
        return ReE(res, err);
    } 
};