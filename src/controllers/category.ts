import { Request, Response } from "express";
import { Category, CategoryDocument } from "../models/Category";
import { ReE, ReS } from "../util/common";

export const findAll = async (req: Request, res: Response) => {
    try {
        const doc: CategoryDocument[] = await Category.find({})
            .populate({path: "image.target", model: "Image", select: "path"})
            .populate({path: "parent", model: "Category", select: "name"})            
            .exec();
        const count: number = await Category.countDocuments({}).exec();
        return ReS(res, {doc, count});
    } catch(err) {
        return ReE(res, err);
    }
};
export const findOne = async (req: Request, res: Response) =>  {
    try {
        const doc: CategoryDocument = await Category.findById(req.params.id).populate({path: "image.target", model: "Image", select: "path"}).exec();
        return ReS(res, {doc});
    } catch(err) {
        return ReE(res, err);
    }  
};
export const create = async (req: Request, res: Response) => {
    try {
        const doc: CategoryDocument = await new Category(req.body).save();
        return ReS(res, {doc});
    } catch(err) {
        return ReE(res, err);
    }  
};
export const update = async (req: Request, res: Response) => {
    try {
        const doc: CategoryDocument = await Category.findByIdAndUpdate(req.params.id, req.body).exec();
        return ReS(res, {doc});
    } catch(err) {
        return ReE(res, err);
    } 
};
export const destroy = async (req: Request, res: Response) => {
    try {

        const doc: CategoryDocument = await Category.findByIdAndRemove(req.params.id).exec();
        return ReS(res, {doc});
    } catch(err) {
        return ReE(res, err);
    } 
};