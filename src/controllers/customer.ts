import { User, UserDocument } from "../models/User";
import { Request, Response  } from "express";

export const findAll = async (req: Request, res: Response) => {
    const doc: UserDocument[] = await User.find({role: "customer"}).exec();

    res.json({success: true, data: doc});
};