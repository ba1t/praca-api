import { Request, Response } from "express";
import { Delivery, DeliveryDocument } from "../models/Delivery";
import { ReE, ReS } from "../util/common";
export const findAll = async (req: Request, res: Response) => {
    try {
        const doc: DeliveryDocument[] = await Delivery.find({}).populate({path: "image.target", model: "Image", select: "path"}).exec();
        const count: number = await Delivery.countDocuments({}).exec();
        return ReS(res, {doc, count});
    } catch(err) {
        return ReE(res, err);
    }
};
export const findOne = async (req: Request, res: Response) =>  {
    try {
        const doc: DeliveryDocument = await Delivery.findById(req.params.id).populate({path: "image.target", model: "Image", select: "path"}).exec();
        return ReS(res, {doc});
    } catch(err) {
        return ReE(res, err);
    }   
};
export const create = async (req: Request, res: Response) => {
    try {
        const doc: DeliveryDocument = await new Delivery(req.body).save();
        return ReS(res, {doc});
    } catch(err) {
        return ReE(res, err);
    } 
};
export const update = async (req: Request, res: Response) => {
    try {
        const doc: DeliveryDocument = await Delivery.findByIdAndUpdate(req.params.id, req.body).exec();
        return ReS(res, {doc});
    } catch(err) {
        return ReE(res, err);
    } 
};
export const destroy = async (req: Request, res: Response) => {
    try {
        const doc: DeliveryDocument = await Delivery.findByIdAndRemove(req.params.id).exec();
        return ReS(res, {doc});
    } catch(err) {
        return ReE(res, err);
    } 
};