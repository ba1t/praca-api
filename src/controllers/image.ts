import { Request, Response } from "express";
import { Image, ImageDocument } from "../models/Image";
import { ReE, ReS } from "../util/common";
import _ from "lodash";
import multer from "../config/multer";
const upload = multer.array("images");

export const findAll = async (req: Request, res: Response) => {
    try {
        const doc: ImageDocument[] = await Image.find({}).populate({path: "image.target", model: "Image", select: "path"}).exec();
        const count: number = await Image.countDocuments({}).exec();
        return ReS(res, {doc, count});
    } catch(err) {
        return ReE(res, err);
    }
};
export const findOne = async (req: Request, res: Response) =>  {
    try {
        const doc: ImageDocument = await Image.findById(req.params.id).populate({path: "image.target", model: "Image", select: "path"}).exec();
        return ReS(res, doc);
    } catch(err) {
        return ReE(res, err);
    }  
};
export const create = async (req: Request, res: Response) => {
    try {
        let doc: ImageDocument = await Image.insertMany(req.files);
        return  ReS(res, {doc});
    } catch(err) {
        return ReE(res, err);
    }  
};
export const destroy = async (req: Request, res: Response) => {
    try {
        const doc: ImageDocument = await Image.findByIdAndRemove(req.params.id).exec();
        return ReS(res, {doc});
    } catch(err) {
        return ReE(res, err);
    }
};