import { Request, Response } from "express";
import { Order, OrderDocument } from "../models/Order";
import { ReE, ReS } from "../util/common";
import _ from "lodash";
const stripe = require("stripe")(process.env.STRIPE_SK);


export const order = async (req: Request, res: Response) => {
    //zmapowanie listy produktów
    const lineItems: any = [];
    _.map(req.body, el => {
        let price = el._id.price * 100;
        lineItems.push({
            // eslint-disable-next-line @typescript-eslint/camelcase
            price_data: {
                currency: "PLN",
                // eslint-disable-next-line @typescript-eslint/camelcase
                product_data: {
                    name: el._id.name.toString(),
                },
                // eslint-disable-next-line @typescript-eslint/camelcase
                unit_amount: price
            },
            quantity: el.count
        });
    });
    const session = await stripe.checkout.sessions.create({
        // eslint-disable-next-line @typescript-eslint/camelcase
        payment_method_types: ["card"],
        // eslint-disable-next-line @typescript-eslint/camelcase
        line_items: lineItems,
        mode: "payment",
        // eslint-disable-next-line @typescript-eslint/camelcase
        success_url: `${process.env.STRIPE_SUCCESS_URL}{CHECKOUT_SESSION_ID}`,
        // eslint-disable-next-line @typescript-eslint/camelcase
        cancel_url: process.env.CANCELL,
    });
    
    res.json({ id: session.id });
};


export const findAll = async (req: Request, res: Response) => {
    try {
        const doc: OrderDocument[] = await Order.find({}).exec();
        const count: number = await Order.countDocuments({}).exec();
        return ReS(res, {doc, count});
    } catch(err) {
        return ReE(res, err);
    }
};
export const findOne = async (req: Request, res: Response) =>  {
    try {
        const doc: OrderDocument = await Order.findById(req.params.id).exec();
        return ReS(res, doc);
    } catch(err) {
        return ReE(res, err);
    }  
};
export const create = async (req: Request, res: Response) => {
    try {
        const doc: OrderDocument = await new Order(req.body).save();
        return ReS(res, doc);
    } catch(err) {
        return ReE(res, err);
    }  
};
export const update = async (req: Request, res: Response) => {
    try {
        const doc: OrderDocument = await Order.findByIdAndUpdate(req.params.id, req.body).exec();
        return ReS(res, doc);
    } catch(err) {
        return ReE(res, err);
    }  
};
export const destroy = async (req: Request, res: Response) => {
    try {
        const doc: OrderDocument = await Order.findByIdAndRemove(req.params.id).exec();
        return ReS(res, doc);
    } catch(err) {
        return ReE(res, err);
    }  
};