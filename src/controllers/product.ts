import { Request, Response } from "express";
import { Product, ProductDocument } from "../models/Product";
import { Category, CategoryDocument } from "../models/Category";
import { ReE, ReS } from "../util/common";
import multer from "../config/multer";
export const findAll = async (req: Request, res: Response) => {
    try {
        const doc: ProductDocument[] = await Product.find()
            .populate({path: "image.target", model: "Image", select: "path"})
            .populate({path: "category", model: "Category"})
            .populate({path: "manufacturer", model: "Manufacturer"})
            .populate({path: "tax", model: "Tax"})
            .populate({path: "created.user", model: "User"})
            .sort({updatedAt: -1})
            .limit(Number(req.query.limit) || 0).exec();
        const count: number = await Product.countDocuments({}).exec();
        return ReS(res, {doc, count});
    } catch(err) {
        return ReE(res, err);
    }
};


export const findAllByCategory = async (req: Request, res: Response) => {
    try {
        const categoryId: CategoryDocument = await Category.findOne({path: req.params.category}).exec();
        try {
            const doc: ProductDocument[] = await Product.find({category: categoryId._id})
                .populate({path: "image.target", model: "Image", select: "path"})
                .populate({path: "category", model: "Category"})
                .populate({path: "manufacturer", model: "Manufacturer"})
                .populate({path: "tax", model: "Tax"})
                .populate({path: "created.user", model: "User"})
                .sort({updatedAt: -1})
                .limit(Number(req.query.limit) || 0).exec();
            const count: number = await Product.countDocuments({}).exec();
            return ReS(res, {doc, count});
        } catch(err) {
            return ReE(res, err);
        }
    } catch(err){
        return ReE(res, err);
    }
    
};

export const findOne = async (req: Request, res: Response) =>  {
    try {
        const doc: ProductDocument = await Product.findById(req.params.id)
            .populate({path: "image.target", model: "Image", select: "path"})
            // .populate({path: "category", model: "Category"})
            // .populate({path: "manufacturer", model: "Manufacturer"})
            // .populate({path: "tax", model: "Tax"})
            // .populate({path: "created.user", model: "User"})
            .exec();
        return ReS(res, {doc});
    } catch(err) {
        return ReE(res, err);
    }  
};
export const create = async (req: Request, res: Response) => {
    try {
        const doc: ProductDocument = await new Product(req.body).save();
        return ReS(res, {doc});
    } catch(err) {
        return ReE(res, err);
    }  
};
export const update = async (req: Request, res: Response) => {
    try {
        const doc: ProductDocument = await Product.findByIdAndUpdate(req.params.id, req.body).exec();
        return ReS(res, {doc});
    } catch(err) {
        return ReE(res, err);
    }  
};
export const destroy = async (req: Request, res: Response) => {
    try {
        const doc: ProductDocument = await Product.findByIdAndRemove(req.params.id).exec();
        return ReS(res, {doc});
    } catch(err) {
        return ReE(res, err);
    }  
};