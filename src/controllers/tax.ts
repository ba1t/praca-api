import { Request, Response } from "express";
import { Tax, TaxDocument } from "../models/Tax";
import { ReE, ReS } from "../util/common";
export const findAll = async (req: Request, res: Response) => {
    try {

        const doc: TaxDocument[] = await Tax.find({}).exec();
        const count: number = await Tax.countDocuments({}).exec();
        return ReS(res, {doc, count});
    } catch(err) {
        return ReE(res, err);
    }  
};
export const findOne = async (req: Request, res: Response) =>  {
    try {
        const doc: TaxDocument = await Tax.findById(req.params.id).exec();
        return ReS(res, {doc});
    } catch(err) {
        return ReE(res, err);
    }  
};
export const create = async (req: Request, res: Response) => {
    try {
        const doc: TaxDocument = await new Tax(req.body).save();
        return ReS(res, {doc});
    } catch(err) {
        return ReE(res, err);
    }  
};
export const update = async (req: Request, res: Response) => {
    try {
        const doc: TaxDocument = await Tax.findByIdAndUpdate(req.params.id, req.body).exec();
        return ReS(res, {doc});
    } catch(err) {
        return ReE(res, err);
    }  
};
export const destroy = async (req: Request, res: Response) => {
    try {
        const doc: TaxDocument = await Tax.findByIdAndRemove(req.params.id).exec();
        return ReS(res, {doc});
    } catch(err) {
        return ReE(res, err);
    }  
};