import mongoose from "mongoose";

export enum Status {
    "active",
    "complete"
}

export type CartDocument = mongoose.Document & {
    status: Status;
    products: {
        _id: mongoose.Schema.Types.ObjectId;
        count: number;
    };
};

const CartSchema = new mongoose.Schema({
    status: {        
        type: String,
        enum: ["enable","disable"]
    },
    products: [
        {
            _id: {type: mongoose.Schema.Types.ObjectId, ref: "Product", unique: true},
            count: {type:Number, default: 1}
        }
    ],
}, { timestamps: true });

export const Cart = mongoose.model<CartDocument>("Cart", CartSchema);

