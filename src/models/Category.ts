import mongoose from "mongoose";

export type CategoryDocument = mongoose.Document & {
    name: string;
    image: {
        target: mongoose.Schema.Types.ObjectId;
        attributes: {
            alt: string; 
            title: string;
        };
    }; 
    parent: string;
    weight: number;
    path: string;
};

const categorySchema = new mongoose.Schema({
    name: { type: String, required: true},
    image: {
        target: { type: mongoose.Schema.Types.ObjectId, ref: "Image" },
        attributes: {
            alt: String, 
            title: String
        }
    }, 
    parent: { type: mongoose.Schema.Types.ObjectId, ref: "Category"},
    weight: {type: Number, default: 0},
    path: { type: String, required: true}
}, { timestamps: true });

export const Category = mongoose.model<CategoryDocument>("Category", categorySchema);

