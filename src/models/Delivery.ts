import mongoose from "mongoose";


export type DeliveryDocument = mongoose.Document & {
    name: string;
    image: {
        target: mongoose.Schema.Types.ObjectId;
        attributes: {
            alt: string; 
            title: string;
        };
    }; 
    price: string;
    weight: number;
};

const deliverySchema = new mongoose.Schema({
    name : { type: String, required: true},
    image: {
        target: { type: mongoose.Schema.Types.ObjectId, ref: "Image" },
        attributes : {
            alt : String, 
            title : String
        }
    }, 
    price: {type: Number, default: 0},
    weight : {type: Number, default: 0}
}, { timestamps: true });

export const Delivery = mongoose.model<DeliveryDocument>("Delivery", deliverySchema);

