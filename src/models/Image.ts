import mongoose from "mongoose";
import multer from "../config/multer";
export type ImageDocument = mongoose.Document & {
    path: string;
    filename: string;
    oryginalname: string;
    size: number;
    mimetype: string;
};

const imageSchema = new mongoose.Schema({
    path: { type: String, required: true},
    filename: String,
    originalname: String,
    size: Number,
    mimetype: String,
}, { timestamps: true });
export const Image = mongoose.model<ImageDocument>("Image", imageSchema);

