import mongoose from "mongoose";


export type ManufacturerDocument = mongoose.Document & {
    name: string;
    image: {
        target: mongoose.Schema.Types.ObjectId;
        attributes: {
            alt: string; 
            title: string;
        };
    };
};

const manufacturerSchema = new mongoose.Schema({
    name: { type: String, required: true},
    image: {
        target: { type: mongoose.Schema.Types.ObjectId, ref: "Image" },
        attributes: {
            alt: String, 
            title: String
        }
    }
}, { timestamps: true });

export const Manufacturer = mongoose.model<ManufacturerDocument>("Manufacturer", manufacturerSchema);

