import mongoose from "mongoose";

export enum Status {
    "przyjęte",
    "oczekuje na płatność",
    "spakowane",
    "wysłane",
};
export type OrderDocument = mongoose.Document & {
    products: object[];
    discount: string;
    invoice: {
        enable: boolean;
        firstName: string;
        lastName: string;
        adress: {
            adress1: string;
            adress2: string;
            adress3: string;
            city: string;
            postalCode: string;
        };
    };
    delivery: {
        firstName: string;
        lastName: string;
        adress: {
            adress1: string;
            adress2: string;
            adress3: string;
            city: string;
            postalCode: string;
        };
    };
    paymentMethod: {
        name: string;
        price: string;
    };
    deliveryMethod: {
        name: string;
        price: string;
    };
    status: Status;
};

const orderSchema = new mongoose.Schema({
    products: [Object],
    discount: String,
    invoice: {
        enable: Boolean,
        firstName: String,
        lastName: String,
        adress: {
            adress1: String,
            adress2: String,
            adress3: String,
            city: String,
            postalCode: String
        }
    },
    delivery: {
        firstName: {type: String, require: true},
        lastName: {type: String, require: true},
        adress: {
            adress1: {type: String, require: true},
            adress2: String,
            adress3: String,
            city: {type: String, require: true},
            postalCode: {type: String, require: true}
        }
    },
    paymentMethod: {
        name: String,
        price: String
    },
    deliveryMethod: {
        name: String,
        price: String
    },
    status: { type: String, enum: [ "przyjęte", "oczekuje na płatność", "spakowane", "wysłane"] }
}, { timestamps: true });

export const Order = mongoose.model<OrderDocument>("Order", orderSchema);

