import mongoose from "mongoose";


export type PaymentDocument = mongoose.Document & {
    name: string;
    image: {
        target: mongoose.Schema.Types.ObjectId;
        attributes: {
            alt: string; 
            title: string;
        };
    }; 
    price: string;
    weight: number;
};

const paymentSchema = new mongoose.Schema({
    name : { type: String, required: true},
    image: {
        target: { type: mongoose.Schema.Types.ObjectId, ref: "Image" },
        attributes : {
            alt : String, 
            title : String
        }
    }, 
    price: String,
    weight : {type: Number, default: 0}
}, { timestamps: true });

export const Payment = mongoose.model<PaymentDocument>("Payment", paymentSchema);

