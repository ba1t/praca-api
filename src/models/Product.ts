import mongoose from "mongoose";

export enum Status {
    "enable",
    "disable"
}
export type ProductDocument = mongoose.Document & {
    name: string;
    body: string;
    enable: boolean;
    status: Status;
    category: mongoose.Schema.Types.ObjectId;
    manufacturer: mongoose.Schema.Types.ObjectId;
    tax: mongoose.Schema.Types.ObjectId;
    created: {
        user: string;
        date: Date;
    };
    stock: number;
    price: string;
    image: {
        target: mongoose.Schema.Types.ObjectId;
        attributes: {
            alt: string; 
            title: string;
        };
    }; 
};

const productSchema = new mongoose.Schema({
    name: {type: String, required: true },
    body: String,
    enable: {type: Boolean, default: true},
    status: {
        type: String,
        enum: ["enable","disable"]
    },
    category: {type: mongoose.Schema.Types.ObjectId, ref: "Category"},
    manufacturer: {type: mongoose.Schema.Types.ObjectId, ref: "Manufacturer"},
    tax: {type: mongoose.Schema.Types.ObjectId, ref: "Tax"},
    created: {
        user: { type: mongoose.Schema.Types.ObjectId, ref: "User"},
        date: { type: Date, default: Date.now }
    },
    stock: { type: Number,  default: 0 },
    price: String,
    image: [{
        target: { type: mongoose.Schema.Types.ObjectId, ref: "Image"},
        attributes: {
            alt: String,
            title: String,
        }
    }],
}, { timestamps: true });

export const Product = mongoose.model<ProductDocument>("Product", productSchema);

