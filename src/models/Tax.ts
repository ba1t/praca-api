import mongoose from "mongoose";


export type TaxDocument = mongoose.Document & {
    name: string;
    rate: number;
};

const taxSchema = new mongoose.Schema({
    name: { type: String, required: true},
    rate: {type: Number, require: true}
}, { timestamps: true });

export const Tax = mongoose.model<TaxDocument>("Tax", taxSchema);

