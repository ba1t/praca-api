import express, {Application} from  "express";
import multer from "../config/multer";

import v1 from "./v1";
import * as imageController from "../controllers/image";
import * as cartController from "../controllers/cart";
const router = express.Router();

export default (app: Application) => {
    app.use((req, res, next) => {
        res.locals.user = req.user;
        next();
    });
    app.use(cartController.createCart);
    app.use("/api/v1",  v1(router));
    app.post("/api/upload", multer.array("images"),imageController.create);
};