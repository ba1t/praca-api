import * as categoryController from "../controllers/category";
import * as deliveryController from "../controllers/delivery";
import * as manufacturerController from "../controllers/manufacturer";
import * as paymentController from "../controllers/payment";
import * as taxController from "../controllers/tax";
import * as productController from "../controllers/product";
import * as orderController from "../controllers/order";
import * as imageController from "../controllers/image";
import * as cartController from "../controllers/cart";
import * as customerController from "../controllers/customer";
import * as userController from "../controllers/user";
import {Request, Response, Router } from "express";
export default (router: Router) => {
    router.post("/api/login", userController.login);
    router.get("/api/logout", userController.logout);
    router.get("/api/profile", userController.profile);
    
    router.post("/api/forgot", userController.forgotPassword);
    
    router.post("/api/reset/:token", userController.reset);
    
    router.post("/api/signup", userController.signup);
    router.post("/api/user/:id", userController.updateProfile);
    
    router.get("/api/auth", (req: Request, res: Response) => {
        if(req.user) {
            res.json({
                loggedIn: true,
                user: req.user
            });
        } else {
            res.json({
                loggedIn: false,
                user: null
            });
        }
    });
    router.get("/category", categoryController.findAll);
    router.get("/category/:id", categoryController.findOne);
    router.post("/category", categoryController.create);
    router.put("/category/:id", categoryController.update);
    router.delete("/category/:id", categoryController.destroy);
    
    router.get("/delivery", deliveryController.findAll);
    router.get("/delivery/:id", deliveryController.findOne);
    router.post("/delivery", deliveryController.create);
    router.put("/delivery/:id", deliveryController.update);
    router.delete("/delivery/:id", deliveryController.destroy);
    
    router.get("/manufacturer", manufacturerController.findAll);
    router.get("/manufacturer/:id", manufacturerController.findOne);
    router.post("/manufacturer", manufacturerController.create);
    router.put("/manufacturer/:id", manufacturerController.update);
    router.delete("/manufacturer/:id", manufacturerController.destroy);
    
    router.get("/payment", paymentController.findAll);
    router.get("/payment/:id", paymentController.findOne);
    router.post("/payment", paymentController.create);
    router.put("/payment/:id", paymentController.update);
    router.delete("/payment/:id", paymentController.destroy);
    
    router.get("/tax", taxController.findAll);
    router.get("/tax/:id", taxController.findOne);
    router.post("/tax", taxController.create);
    router.put("/tax/:id", taxController.update);
    router.delete("/tax/:id", taxController.destroy);
    
    router.get("/product", productController.findAll);
    router.get("/product/category/:category", productController.findAllByCategory);
    router.get("/product/:id", productController.findOne);
    router.post("/product", productController.create);
    router.put("/product/:id", productController.update);
    router.delete("/product/:id", productController.destroy);
    
    router.post("/confirm", orderController.order);

    router.get("/order", orderController.findAll);
    router.get("/order/:id", orderController.findOne);
    router.post("/order", orderController.create);
    router.put("/order/:id", orderController.update);
    router.delete("/order/:id", orderController.destroy);
    
    router.get("/image", imageController.findAll);

    router.get("/getCart", cartController.getFrontCart);
    router.get("/cart", cartController.findAll);
    router.get("/cart/view/:id", cartController.findOne);
    router.post("/cart/add/:id", cartController.addToCart);
    router.post("/cart/remove/:id", cartController.removeFromCart);

    router.get("/customer", customerController.findAll);

    return router;
};
