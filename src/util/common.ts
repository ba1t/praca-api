import { Response } from "express";
import _ from "lodash";
export const ReE = function(res: Response, err: any, code: number = 500): Response { 
    if(typeof err == "object" && typeof err.message != "undefined"){
        err = err.message;
    }

    return res.status(code).json({success:false, error: err});
};

export const ReS = function(res: Response, data: any, code: number = 200): Response {
    let sendData = {success:true};

    if(typeof data == "object"){
        sendData = _.merge(data, sendData);
    }

    return res.status(code).json(sendData);
};

export const TE = function(errMessage: any, log: boolean = true): Error { 
    if(log === true){
        console.error(errMessage);
    }

    throw new Error(errMessage);
};