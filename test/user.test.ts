import request from "supertest";
import app from "../src/app";
import { expect } from "chai";

describe("POST /login", () => {
    it("logowanie do systemu", (done) => {
        return request(app).post("/login")
            .field("email", "bait@onet.pl")
            .field("password", "bait123")
            .expect(302)
            .end(function(err, res) {
                expect(res.error).not.to.be.undefined;
                done();
            });

    });
});
